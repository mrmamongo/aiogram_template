from dataclasses import dataclass


@dataclass
class LoggingConfig:
    log_level: str
    format: str


@dataclass
class DatabaseConfig:
    @property
    def dsn(self):
        return "postgres://postgres:postgres@localhost:5432"


@dataclass
class TelegramConfig:
    token: str
    webhook_url: str

@dataclass
class APIConfig:
    host: str
    port: int

@dataclass
class Config:
    telegram: TelegramConfig
    logging: LoggingConfig
    database: DatabaseConfig
    api: APIConfig

def get_config():
    """Получить конфигурацию из .env"""
