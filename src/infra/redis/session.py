from redis.asyncio import Redis, from_url


async def create_redis(url: str) -> Redis:
    return await from_url(url)
