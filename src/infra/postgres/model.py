from dataclasses import dataclass


@dataclass
class User:
    db_id: str
    telegram_id: str
    username: str
    is_admin: bool