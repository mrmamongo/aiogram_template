import asyncpg

from src.config import DatabaseConfig


async def setup_database(config: DatabaseConfig):
    return await asyncpg.create_pool(dsn=config.dsn)
