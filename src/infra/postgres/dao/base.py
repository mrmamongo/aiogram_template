import logging

import asyncpg


class BaseDAO:
    def __init__(self, connection: asyncpg.Connection) -> None:
        self.connection = connection
        self.logger = logging.getLogger(self.__class__.__name__)
