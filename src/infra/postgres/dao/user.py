from src.application.user.interfaces import UserDAO
from src.infra.postgres.dao.base import BaseDAO
from src.application.user.dto import UserRead


class DBUserDAO(BaseDAO, UserDAO):
    async def get_by_telegram(self, telegram_id: int) -> UserRead | None:
        user = self.connection.fetchrow("...")
        # Как то там сконвертировать
        return UserRead(user)

    # ... Остальные методы CRUD ...
