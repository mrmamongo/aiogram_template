from aiogram.filters import Filter
from aiogram.types import Message

from src.application.user.dto import UserRead
from src.application.user.interfaces import UserDAO


class AdminFilter(Filter):
    def __call__(self, message: Message, user_dao: UserDAO) -> bool:
        user: UserRead | None = await user_dao.get_by_telegram(message.from_user.id)

        if user is None:
            return False

        return user.is_admin
