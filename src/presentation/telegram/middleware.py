from typing import Callable, Dict, Any, Awaitable

from aiogram.dispatcher.middlewares.base import BaseMiddleware
from aiogram.types import TelegramObject
from asyncpg import Connection

from src.application.user.interfaces import UserDAO
from src.infra.postgres.dao.user import DBUserDAO
from src.presentation.telegram.exceptions import DependencyNotFoundException


class UserDAOMiddleware(BaseMiddleware):
    async def __call__(self, handler: Callable[[TelegramObject, Dict[str, Any]], Awaitable[Any]], event: TelegramObject,
                       data: Dict[str, Any]) -> Any:
        if "pool" not in data:
            raise DependencyNotFoundException(Connection, DBUserDAO)

        data['user_dao']: UserDAO = DBUserDAO(data['pool'])
        return await handler(event, data)
