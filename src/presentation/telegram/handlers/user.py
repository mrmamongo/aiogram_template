import logging

from aiogram import types, Router
from aiogram.filters import CommandStart

from src.application.user.dto import UserRead
from src.application.user.interfaces import UserDAO

logger = logging.getLogger("User Handler")

router = Router()


@router.message(CommandStart())
async def start(message: types.Message, user_dao: UserDAO):
    user: UserRead = await user_dao.get_by_telegram(message.from_user.id)
    logger.info(f"User {user.username} started bot")
    await message.answer(f"Hello, {user.username}! {message.from_user.id}")
