from typing import Type


class DependencyNotFoundException(Exception):
    def __init__(self, dependency_class: Type, dependant_class: Type):
        self.dependency_class = dependency_class
        self.dependant_class = dependant_class

    def __str__(self):
        return f"Dependency {self.dependency_class.__class__} not found by {self.dependant_class.__class__}"
