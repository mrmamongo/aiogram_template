from aiogram import Dispatcher, Bot

from src.presentation.telegram.filters import AdminFilter
from src.presentation.telegram.handlers.user import router as user_router
from src.presentation.telegram.handlers.admin import router as admin_router


def setup_telegram(bot: Bot, dp: Dispatcher) -> None:
    dp.include_router(user_router)

    admin_router.message.filter(AdminFilter())
    dp.include_router(admin_router)
