from aiogram import Bot, Dispatcher
from pydantic import BaseModel


class BotDependency(BaseModel):
    bot: Bot
    dp: Dispatcher
