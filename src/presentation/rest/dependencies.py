from fastapi.requests import Request

from src.presentation.rest.scheme import BotDependency


def bot_scope(request: Request) -> BotDependency:
    return request.state.bot()
