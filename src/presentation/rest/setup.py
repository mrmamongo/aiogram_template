from aiogram import Bot, Dispatcher
from fastapi import FastAPI, APIRouter
from starlette import status

from src.config import Config
from src.presentation.rest.scheme import BotDependency
from src.presentation.rest.telegram import process_update


def setup_routers(app: FastAPI, bot: Bot, dp: Dispatcher, config: Config) -> None:
    app.state.bot = lambda: BotDependency(bot=bot, dp=dp)

    telegram_router = APIRouter()
    telegram_router.post(path=config.telegram.webhook_url, status_code=status.HTTP_200_OK)(process_update)

    # setup other routers...
