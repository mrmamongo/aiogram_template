from typing import Annotated

from fastapi import Depends

from src.presentation.rest.dependencies import bot_scope
from src.presentation.rest.scheme import BotDependency


async def process_update(update: dict, bot: Annotated[BotDependency, Depends(bot_scope)]):
    await bot.dp.feed_raw_update(bot.bot, update)
