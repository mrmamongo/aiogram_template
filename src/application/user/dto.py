from dataclasses import dataclass


@dataclass
class UserRead:
    telegram_id: str
    username: str
    is_admin: bool
