from abc import ABC, abstractmethod

from src.application.user.dto import UserRead


class UserDAO(ABC):
    @abstractmethod
    async def get_by_telegram(self, telegram_id: int) -> UserRead | None:
        pass
