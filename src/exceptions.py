class ServiceStartException(Exception):
    pass


class ServiceDisposeException(Exception):
    pass
