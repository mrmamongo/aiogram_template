import asyncio
import logging

import uvicorn
from aiogram import Bot, Dispatcher
from fastapi import FastAPI

from src.config import Config
from src.exceptions import ServiceStartException, ServiceDisposeException
from src.infra.postgres.session import setup_database
from src.presentation.rest.setup import setup_routers
from src.presentation.telegram.middleware import UserDAOMiddleware

logger = logging.getLogger(__name__)


class WebAppTelegram:
    def __init__(self, bot: Bot, dispatcher: Dispatcher, fastapi: FastAPI, config: Config) -> None:
        self.bot = bot
        self.dispatcher = dispatcher
        self.fastapi = fastapi
        self.config = config

    @staticmethod
    def from_config(config: Config) -> "WebAppTelegram":
        bot = Bot(config.telegram.token)
        dp = Dispatcher()
        app = FastAPI()

        logger.info("Setting up webhook and web app")
        setup_routers(app, bot, dp, config)

        logger.info("Initializing dependencies")
        pool = await setup_database(config.database)
        dp["pool"] = pool

        dp.update.outer_middleware(UserDAOMiddleware())
        return WebAppTelegram(bot=bot, dispatcher=dp, fastapi=app, config=config)

    async def start(self) -> None:
        logger.info("Starting service...")

        try:
            await self.bot.set_webhook(self.config.telegram.webhook_url)

            server = uvicorn.Server(
                config=uvicorn.Config(app=self.fastapi, host=self.config.api.host, port=self.config.api.port))
            await server.serve()

        except asyncio.CancelledError:
            logger.info("Service interrupted")
        except BaseException as unexpected:
            logger.exception("Service failed to start")
            raise ServiceStartException from unexpected

    async def dispose(self) -> None:
        logger.info("Stopping service...")

        dispose_errors: list[str] = []

        # TODO: dispose connections, clear namespaces etc
        try:
            await self.bot.delete_webhook(drop_pending_updates=True)
        except Exception as e:
            dispose_errors.append(str(e))

        try:
            await self.bot.close()
        except Exception as e:
            dispose_errors.append(str(e))

        if len(dispose_errors) != 0:
            raise ServiceDisposeException("Service has shut down with errors, see logs above")

        logger.info("Service has successfully shut down")
