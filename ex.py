import asyncio
from typing import Callable, Dict, Any, Awaitable

from aiogram import Dispatcher, Bot, BaseMiddleware
from aiogram.filters import Filter, CommandStart
from aiogram.types import Message, TelegramObject

bot = Bot(token="jopa")
dp = Dispatcher()


class AdminMiddleware(BaseMiddleware):
    async def __call__(self, handler: Callable[[TelegramObject, Dict[str, Any]], Awaitable[Any]], event: TelegramObject,
                       data: Dict[str, Any]) -> Any:
        print(data["pool"])


class AdminFilter(Filter):
    async def __call__(self, message: Message, pool: object) -> bool:
        print(pool)
        return True


@dp.message(CommandStart())
@dp.message()
async def start(message: Message) -> None:
    await message.answer("Halo")


async def main() -> None:
    dp['pool'] = object()
    dp.message.filter(AdminFilter())
    dp.message.middleware(AdminMiddleware())
    await bot.delete_webhook(drop_pending_updates=True)
    await dp.start_polling(bot)


if __name__ == '__main__':
    asyncio.run(main())
